# ShellSort algorithm
> Implementation ShellSort in Python

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Project Status](#project-status)
* [Contact](#contact)


## General Information
- This project was realized on III semester of studies.
- Project include ShellSort algorithm in Python language.
- The purpose of project was to learn Python and algorithm & data structures.
- Program include a function to test algorithm many times


## Technologies Used
- Python - version 3.9
- PyCharm Community Edition


## Features
Provided features:
- Implementation of ShellSort
- Testing function with output to a txt file


## Project Status
Project is: _no longer being worked on_. 


## Contact
Created by Piotr Hajduk