import random
import time
import math




def ShellSort(A):
    tab = []
    h = 1
    wyk = 1
    while h <= len(A):
        h = (2 ** wyk) - 1
        wyk = wyk + 1
        if h <= len(A):
            tab.append(h)
    next_step = len(tab) - 1
    while next_step >= 0:
        for i in range(tab[next_step], len(A)):
            x = A[i]
            j = i - tab[next_step]
            while j >= 0 and x <= A[j]:
                A[j + tab[next_step]] = A[j]
                j = j - tab[next_step]
            A[j + tab[next_step]] = x
        next_step = next_step - 1


def verify_alg(A):
    suma = 0
    for i in range(len(A) - 1):
        if A[i] <= A[i + 1]:
            suma = suma + 1
    if suma + 1 == len(A):
        return True
    else:
        return False


def generator(N):
    A = []
    for liczba in range(N):
        A.append(random.randint(-500, 500))
    return A
def odchylenie(czasy):
    odch = 5
    return odch


testy = [500, 750, 1000, 2000, 3000, 5000, 10000, 15000, 20000, 25000, 30000,
         35000, 40000, 45000, 50000, 55000, 60000, 65000, 70000, 75000, 80000,
         85000, 90000, 95000, 100000]
file = open('Testy_3_sprawko2.txt', 'w')
for i in range(len(testy)):
    czasy = []
    suma = 0
    suma_odch = 0
    for k in range(20):
        obecny = generator(testy[i])
        tp = time.time()
        ShellSort(obecny)
        tk = time.time()
        if verify_alg(obecny):
            tc = tk - tp
            czasy.append(tc)
            suma = suma + tc
        else:
            print('Algorytm nie zadzialal')
    czas_sr = suma/len(czasy)
    for czas in czasy:
        kwadrat = math.pow(czas - czas_sr, 2)
        suma_odch = suma_odch + kwadrat
    odch_stand = math.sqrt(suma_odch/len(czasy))
    file.write(f'{czas_sr}\n')
   # file.write(f'Czas sredni algorytmu dal N = {testy[i]} wynosi\t{czas_sr} \n')
   # file.write(f'Odchylenie standardowe algorytmu dla N = {testy[i]} wynosi\t{odch_stand} \n\n')
    print(f'Czas sredni algorytmu dal N = {testy[i]} wynosi\t{czas_sr} ')
    print(f'Odchylenie standardowe algorytmu dla N = {testy[i]} wynosi\t{odch_stand} ')
file.close()

